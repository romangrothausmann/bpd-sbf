
#### Notes:
##


#### ToDo:


#### Done:



SUBMODDIR?=../submodules/


SHELL:= /bin/bash -O extglob


### setting default paths of external programs
ITK?=$(SUBMODDIR)/ITK-CLIs/build
VTK?=$(SUBMODDIR)/VTK-CLIs/build
ITKVTK?=$(SUBMODDIR)/ITK-VTK_CLIs/build
SNAP?=/opt/itksnap-3.4_QT4_gdWS/
PV?=/opt/ParaView-5.6.0-MPI-Linux-64bit/
GP?=/opt/gnuplot-5.0.3/
VGL?=/opt/VirtualGL-2.6.2/

VGLRUN?=vglrun

export PATH:= $(ITK):$(PATH)
export PATH:= $(VTK):$(PATH)
export PATH:= $(ITKVTK):$(PATH)
export PATH:= $(SNAP)/bin:$(PATH)
export PATH:= $(PV)/bin:$(PATH)
export PATH:= $(GP)/bin:$(PATH)
export PATH:= $(VGL)/bin:$(PATH)

export JAVA_FLAGS=-Xmx10g

### list internal programs
EXECUTABLES = file_converter file-series_reader subimage_extract_series subimage_extract resample resample-iso std-mean_ROI_SBS median_SDI anisoDiff-grad_f32 gradient_mag_f32 watershed_morph_f32_UI32 toUInt8 shift-scale_window_UI8 thresh-glob fill_holes stat erode-dilate_dm_f32 mean
EXECUTABLES+= marching-cubes mc_discrete probe-surf threshold vtp2ply
### also check uncommon tools
EXECUTABLES+= itksnap
EXECUTABLES+= parallel
EXECUTABLES+= $(VGLRUN)

## run IM tools without setting cration and modification date (or -strip) to avoid changes in PNGs that are not visual: http://stackoverflow.com/questions/13577280/getting-imagemagick-convert-to-not-write-out-extra-info
## some bug now needs even more default options: http://unix.stackexchange.com/questions/255252/create-the-same-png-with-imagemagick-2-times-binaries-differ#255256
convertIM:= convert-im6.q16hdri -define png:exclude-chunks=date,time +set date:create +set date:modify
mogrifyIM:= mogrify -define png:exclude-chunks=date,time +set date:create +set date:modify


### check existance of external programs
K:= $(foreach exec,$(EXECUTABLES),\
	$(if $(shell PATH=$(PATH) which $(exec)),some string,$(error "No $(exec) in PATH")))


BN = BPD
dZ = 0.000080

nZ02 = 4324
RAW02= $(shell seq -f %04.0f 0 $(nZ02))

nZ03 = 5020
RAW03= $(shell seq -f %04.0f 0 $(nZ03))

nZ04 = 5062
RAW04= $(shell seq -f %04.0f 0 $(nZ04))

nZ05 = 5025
RAW05= $(shell seq -f %04.0f 0 $(nZ05))

nZ06 = 5098
RAW06= $(shell seq -f %04.0f 0 $(nZ06))


SPACE := $(eval) $(eval)
base_ = $(subst $(SPACE),_,$(filter-out $(lastword $(subst _, ,$1)),$(subst _, ,$1)))
base. = $(subst $(SPACE),.,$(filter-out $(lastword $(subst ., ,$1)),$(subst ., ,$1)))
seg_ = $(subst seg_,,$1)



.PHONY : all clean  BPD-02 BPD-03 BPD-04 BPD-05 BPD-06

all : BPD-02 BPD-03 BPD-04 BPD-05 BPD-06


BPD-02 : BPD-02/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh_m.vtp
BPD-02 : BPD-02/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m.vtp
BPD-02 : BPD-02/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m_alvD.vtp

BPD-03 : BPD-03/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh_m.vtp
BPD-03 : BPD-03/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m.vtp
BPD-03 : BPD-03/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m_alvD.vtp

BPD-04 : BPD-04/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh_m.vtp
BPD-04 : BPD-04/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m.vtp
BPD-04 : BPD-04/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m_alvD.vtp

BPD-05 : BPD-05/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh_m.vtp
BPD-05 : BPD-05/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m.vtp
BPD-05 : BPD-05/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m_alvD.vtp

BPD-06 : BPD-06/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh_m.vtp
BPD-06 : BPD-06/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m.vtp
BPD-06 : BPD-06/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_m_alvD.vtp


clean :
	-rm -v !(Makefile)

clean4annex :
	$(MAKE) -ndr | awk  '/Must remake target/ {print $$4}' | sed "s/'\.$$//g;s/'//g;/^all$$/d" | xargs rm -v


% :: %.gz # try decompress-first rule
	unpigz -v -- $<

%.gz : %
	pigz -v -- $< 

%.tif : %.mha # for MIB
	file_converter $< $@ 0

%.mrc : %.mha # for IMOD
	file_converter $< $@ 0


BPD-02/$(BN).mha : $(RAW02:%=../raw/BPD-02/%.dm3)

BPD-02/%_ROI01.mha : BPD-02/%.mha
	subimage_extract $< $@ 100  7120 816 249  6880 6000 900

BPD-02/%_ROI02.mha : BPD-02/%_ROI01.mha
	subimage_extract $< $@ 100  1058 773   0  5820 4590 900

BPD-02/%_smROI.mha : BPD-02/%.mha
	std-mean_ROI_SBS $< $@  100 100 0  2300 1800 900


BPD-03/$(BN).mha : $(RAW03:%=../raw/BPD-03/%.dm3)

BPD-03/%_ROI01.mha : BPD-03/%.mha
	subimage_extract $< $@ 100  6600 3900 805  3900 3100  721

BPD-03/%_ROI02.mha : BPD-03/%.mha
	subimage_extract $< $@ 100  6600 3900 805  3900 3100 1200

BPD-03/%_smROI.mha : BPD-03/%.mha
	std-mean_ROI_SBS $< $@  100 100 0  1500 1100 721


BPD-04/$(BN).mha : $(RAW04:%=../raw/BPD-04/%.dm3)

BPD-04/%_ROI01.mha : BPD-04/%.mha
	subimage_extract $< $@ 100  1500 3800 480  4000 5000 700

BPD-04/%_smROI.mha : BPD-04/%.mha
	std-mean_ROI_SBS $< $@  100 100 0  1500 2000 700


BPD-05/$(BN).mha : $(RAW05:%=../raw/BPD-05/%.dm3) # needs ITK to be compiled with SCIFIO

BPD-05/%_ROI01.mha : BPD-05/%.mha
	subimage_extract $< $@ 100  2400 1700 3400  2400 2000 600

BPD-05/%_ROI02.mha : BPD-05/%.mha
	subimage_extract $< $@ 100 10900 1000 3600  2700 3200 660

BPD-05/$(BN)_ROI01_+1+0.000080rsi_smROI.mha : BPD-05/%_smROI.mha : BPD-05/%.mha
	std-mean_ROI_SBS $< $@  100 100 0   800  700 600

BPD-05/$(BN)_ROI02_+1+0.000080rsi_smROI.mha : BPD-05/%_smROI.mha : BPD-05/%.mha
	std-mean_ROI_SBS $< $@  100 100 0  1000 1200 660


BPD-06/$(BN).mha : $(RAW06:%=../raw/BPD-06/%.dm3)

BPD-06/%_ROI00.mha : BPD-06/%.mha
	subimage_extract $< $@ 100  9800 9200 2250  4000 3000 1000

BPD-06/%_ROI01.mha : BPD-06/%_ROI00.mha
	subimage_extract $< $@ 100     0    0  220  4000 3000  770

BPD-06/%_smROI.mha : BPD-06/%.mha
	std-mean_ROI_SBS $< $@  100 100 0  1550 1110  770


BPD-%/$(BN).mha : # needs ITK to be compiled with SCIFIO
	JAVA_HOME=$$(readlink -f /usr/bin/java | sed "s:/bin/java::") \
	file-series_reader $@ $(dZ) $^  # pixel size in mm

%_md.mha : %.mha
	median_SDI $< $@  100  0 0 1

%_gm.mha : %.mha
	gradient_mag_f32 $< $@ 0

%_ssw.mha : %.mha
	shift-scale_window_UI8 $< $@  100  20000 40000

BPD-02/seg/$(BN)_ROI02_+1+$(dZ)rsi_man.mha : BPD-02/$(BN)_ROI02_+1+$(dZ)rsi_smROI_md_+100+0.9adg_gm_+400000wsm.mha BPD-02/$(BN)_ROI02_+1+$(dZ)rsi_smROI_md.mha

BPD-03/seg/$(BN)_ROI01_+1+$(dZ)rsi_man.mha : BPD-03/$(BN)_ROI01_+1+$(dZ)rsi_smROI_md_+100+0.9adg_gm_+400000wsm.mha BPD-03/$(BN)_ROI01_+1+$(dZ)rsi_smROI_md.mha

BPD-04/seg/$(BN)_ROI01_+1+$(dZ)rsi_man.mha : BPD-04/$(BN)_ROI01_+1+$(dZ)rsi_smROI_md_+100+0.9adg_gm_+1350000wsm.mha BPD-04/$(BN)_ROI01_+1+$(dZ)rsi_smROI_md.mha

BPD-05/seg/$(BN)_ROI01_+1+$(dZ)rsi_man.mha : BPD-05/$(BN)_ROI01_+1+$(dZ)rsi_smROI_md_+100+0.9adg_gm_+1500000wsm.mha BPD-05/$(BN)_ROI01_+1+$(dZ)rsi_smROI_md.mha

BPD-06/seg/$(BN)_ROI01_+1+$(dZ)rsi_man.mha : BPD-06/$(BN)_ROI01_+1+$(dZ)rsi_smROI_md_+100+0.9adg_gm_+100000wsm.mha BPD-06/$(BN)_ROI01_+1+$(dZ)rsi_smROI_md.mha

BPD-%/seg/$(BN)_ROI01_+1+$(dZ)rsi_man.mha : 
	@echo "process $@"
	$(VGLRUN) itksnap $(subst §,$(SPACE),$(join $(addsuffix §, -s -g),$^)) # space eaten by join, so use § as a place holder
	toUInt8 $@ $@ 0
#	touch $@

%_man.mha : seg/%_man.mha
	ln -sf seg/$(notdir $<) $@

%_b.mha : %.mha
	thresh-glob $< $@ 100 1 1

%_A.mha : %.mha
	thresh-glob $< $@ 100 2 2

%_fh.mha : %.mha
	fill_holes $< $@ 0 0

%_m.mha : %.mha
	mean $< $@ 0  2

%_m.vtp : %_m.mha
	marching-cubes $< $@ 0 127

%.vtp : %.mha
	mc_discrete $< $@ 0 1 ` stat $< 2>&1 | awk '/Max/{print $$4}' ` 0 # 2^15 - 1 is max for dmc

%_b_fh_m_alvD.vtp : %_b_fh_m.vtp %_A_fh_0.020+ed.mha 
	probe-surf $^ $@ 0 0
	threshold $@ $@ 0  0 MetaImage 255 255 1

%.ply : %.vtp
	vtp2ply $< $@

BPD-SBF.pvsm : BPD-03/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh.vtp
BPD-SBF.pvsm : BPD-03/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh.vtp
BPD-SBF.pvsm : BPD-03/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_alvD.vtp

BPD-SBF.pvsm : BPD-04/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh.vtp
BPD-SBF.pvsm : BPD-04/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh.vtp
BPD-SBF.pvsm : BPD-04/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_alvD.vtp

BPD-SBF.pvsm : BPD-05/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh.vtp
BPD-SBF.pvsm : BPD-05/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh.vtp
BPD-SBF.pvsm : BPD-05/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_alvD.vtp

BPD-SBF.pvsm : BPD-06/$(BN)_ROI01_+1+$(dZ)rsi_man_A_fh.vtp
BPD-SBF.pvsm : BPD-06/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh.vtp
BPD-SBF.pvsm : BPD-06/$(BN)_ROI01_+1+$(dZ)rsi_man_b_fh_alvD.vtp

BPD-SBF.pvsm : 
	touch $@


## prevent removal of any intermediate files
.SECONDARY: 

.SECONDEXPANSION:

%adg.mha : $$(call base_,%).mha
	@echo "process $+ to $*"
	$(eval v= $(subst $(basename $<),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst adg,,$(v)))
	$(eval sep= $(subst +, ,$(v)))
	$(eval it= $(firstword $(sep)))
	$(eval co= $(lastword $(sep)))
	anisoDiff-grad_f32 $< $@ 0  $(it) 5e-06 $(co)

%wsm.mha : $$(call base_,%).mha
	$(eval v= $(subst $(basename $<),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst wsm,,$(v)))
	watershed_morph_f32_UI32 $< $@ 0  $(v) 1 0 0

%rsi.mha : $$(call base_,%).mha
	@echo "process $+ to $*"
	$(eval v= $(subst $(basename $<),,$*))
#	$(eval v= $(subst $(basename $+),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst rs,,$(v)))
	$(eval sep= $(subst +, ,$(v)))
	$(eval itp= $(firstword $(sep)))
	$(eval siz= $(lastword $(sep)))

	resample-iso $+ $@ 100  $(itp) $(siz)

%rs.mha : $$(call base_,%).mha
	@echo "process $+ to $*"
	$(eval v= $(subst $(basename $<),,$*))
#	$(eval v= $(subst $(basename $+),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst rs,,$(v)))
	$(eval sep= $(subst +, ,$(v)))
	$(eval itp= $(firstword $(sep)))
	$(eval siz= $(lastword $(sep)))

	resample $+ $@ 100  $(itp) $(siz) $(siz) $(siz)

%ed.mha : $$(call base_,%).mha
	$(eval v= $(subst $(basename $<),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst ed,,$(v)))
	erode-dilate_dm_f32 $< $@ 0  $(v) 1 # use image spacing


%r.png : $$(call base_,%).pvsm
	@echo "process $+ to $*"
	$(eval v= $(subst $(basename $<),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst r,,$(v)))
	$(eval sep= $(subst +, ,$(v)))

	$(VGLRUN) pvpython $(SUBMODDIR)/paraview/scripts/render-view.py  -s $(sep) -hq -i $< -png $@

%rc.png : $$(call base_,%).pvsm $$(call base_,%).pvcc
	@echo "process $+ to $*"
	$(eval v= $(subst $(basename $<),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst rc,,$(v)))
	$(eval sep= $(subst +, ,$(v)))

	$(VGLRUN) pvpython $(SUBMODDIR)/paraview/scripts/render-view.py  -s $(sep) -hq -i $< -c $(lastword $^) -png $@
